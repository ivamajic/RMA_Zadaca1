package hr.ferit.ivamajic.zadaca1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

class InspiringPerson {
    private String name;
    private String date;
    private String description;

    //konstruktor
    InspiringPerson(String n, String d, String desc) {
        name=n;
        date=d;
        description=desc;
    }

    //getteri
    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getDesc() {
        return description;
    }

    //setteri
    public void setName(String n) {
        name=n;
    }

    public void setDate(String d) {
        date=d;
    }

    public void setDescription(String desc) {
        description=desc;
    }
}

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ivLovelace)ImageView Lovelace;
    @BindView(R.id.ivNeumann)ImageView Neumann;
    @BindView(R.id.ivTorvalds)ImageView Torvalds;

    @BindView(R.id.tvLovelaceName) TextView tvLovelaceName;
    @BindView(R.id.tvLovelaceDate) TextView tvLovelaceDate;
    @BindView(R.id.tvLovelaceText) TextView tvLovelaceText;

    @BindView(R.id.tvNeumannName) TextView tvNeumannName;
    @BindView(R.id.tvNeumannDate) TextView tvNeumannDate;
    @BindView(R.id.tvNeumannText) TextView tvNeumannText;

    @BindView(R.id.tvTorvaldsName) TextView tvTorvaldsName;
    @BindView(R.id.tvTorvaldsDate) TextView tvTorvaldsDate;
    @BindView(R.id.tvTorvaldsText) TextView tvTorvaldsText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);


        String name1=getString(R.string.lovelaceName);
        String name2=getString(R.string.neumannName);
        String name3=getString(R.string.torvaldsName);

        String date1=getString(R.string.lovelaceDate);
        String date2=getString(R.string.neumannDate);
        String date3=getString(R.string.torvaldsDate);

        String text1=getString(R.string.lovelaceText);
        String text2=getString(R.string.neumannText);
        String text3=getString(R.string.torvaldsText);

        InspiringPerson Lovelace = new InspiringPerson(name1, date1, text1);
        InspiringPerson Neumann = new InspiringPerson(name2, date2, text2);
        InspiringPerson Torvalds = new InspiringPerson(name3, date3, text3);

        tvLovelaceName.setText(Lovelace.getName());
        tvLovelaceDate.setText(Lovelace.getDate());
        tvLovelaceText.setText(Lovelace.getDesc());

        tvNeumannName.setText(Neumann.getName());
        tvNeumannDate.setText(Neumann.getDate());
        tvNeumannText.setText(Neumann.getDesc());

        tvTorvaldsName.setText(Torvalds.getName());
        tvTorvaldsDate.setText(Torvalds.getDate());
        tvTorvaldsText.setText(Torvalds.getDesc());
    }

     @OnClick(R.id.ivLovelace)
    public void prikaziPorukuLovelace() {
        Random rand=new Random();
        int rnd=rand.nextInt(3 ) + 1;
        String quote;
        switch (rnd) {
            case 1:
                quote=getString(R.string.lovelaceQuote1);
                break;
            case 2:
                quote=getString(R.string.lovelaceQuote2);
                break;
            default:
                quote=getString(R.string.lovelaceQuote3);
                break;
        }
         Toast.makeText(this, quote, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.ivNeumann)
    public void prikaziPorukuNemann() {
        Random rand=new Random();
        int rnd=rand.nextInt(3 ) + 1;
        String quote;
        switch (rnd) {
            case 1:
                quote=getString(R.string.neumannQuote1);
                break;
            case 2:
                quote=getString(R.string.neumannQuote2);
                break;
            default:
                quote=getString(R.string.neumannQuote3);
                break;
        }
        Toast.makeText(this, quote, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.ivTorvalds)
    public void prikaziPorukuTorvalds() {
        Random rand=new Random();
        int rnd=rand.nextInt(3 ) + 1;
        String quote;
        switch (rnd) {
            case 1:
                quote=getString(R.string.torvaldsQuote1);
                break;
            case 2:
                quote=getString(R.string.torvaldsQuote2);
                break;
            default:
                quote=getString(R.string.torvaldsQuote3);
                break;
        }
        Toast.makeText(this, quote, Toast.LENGTH_SHORT).show();
    }

}


