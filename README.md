# Homework project 1 - Inspiring Person App

### The assignment and problems encountered

* The assignment was to make an app that shows images and short bios of three inspiring people from the computer science history.

* I added the *InspiringPerson* class with getters and setters and created three objects, one for each person.
* Using the *getString()* method, getters and *setText()* method, I set the TextViews (name, date of birth and death, bio) for each person.
* When an image is clicked, a random quote is supposed to show as a Toast message. I found three quotes for each person. The quote is chosen by a pseudorandom number generator and *switch-case*. This was the biggest problem since I, a Java beginner, didn't know how the random function works in Java.


### Utilised libraries

I used [Butterknife] (http://jakewharton.github.io/butterknife/) to bind views and make my code simpler.

### Screenshots

![screenshot1](Screenshots/screenshot1.png)
![screenshot2](Screenshots/screenshot2.png)
![screenshot3](Screenshots/screenshot3.png)
![screenshot4](Screenshots/screenshot4.png)
![screenshot5](Screenshots/screenshot5.png)
![screenshot6](Screenshots/screenshot6.png)


### Completed Tasks

- [x] Make a new Android project with one Activity
- [x] Add InspiringPerson class
- [x] Add getters and setters to class
- [x] Define XML interface with one root layout
- [x] Write a method for showing quotes
- [x] Create an icon for the app. 
- [x] Test the app *The app was tested on Samsung Galaxy S7 (API 24) and Nexus 4 virtual device (API 22).*